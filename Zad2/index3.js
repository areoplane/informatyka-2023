// Tworzymy tablicę, która będzie przechowywać kolory dla każdej komórki
const colors = [];

for (let i = 1; i <= 100; i++) {
  const row = document.createElement("tr");
  for (let j = 1; j <= 100; j++) {
    const cell = document.createElement("td");
    const color = getRandomColor();
    cell.textContent = i * j;
    colors.push(color);
    cell.style.backgroundColor = color;
    row.appendChild(cell);
  }
  document.getElementById("tabliczka-mnozenia").appendChild(row);
}

function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}